<?php

namespace Todo\Storage;

use Todo\Models\Task;
use Todo\Storage\Contracts\TaskStorageInterface;

class MySqlDatabaseTaskStorage implements TaskStorageInterface {

	protected $db;

	public function __construct(\PDO $db) {
		$this->db = $db;
	}

	public function store(Task $task) {
		$stm = $this->db->prepare("
			INSERT INTO tasks (description, due, complete)
			VALUES (:description, :due, :complete)
		");
		$stm->execute($this->buildColumns($task));

		return $this->get($this->db->lastInsertId());
	}

	public function update(Task $task) {
		$stm = $this->db->prepare("
			UPDATE tasks
			SET
				description = :description,
				due = :due,
				complete = :complete
			WHERE id = :id
		");
		$stm->execute($this->buildColumns($task, [
			'id' => $task->getId(),
		]));

		return $this->get($task->getId());
	}

	public function get($id) {
		$stm = $this->db->prepare("
			SELECT id, description, due, complete
			FROM tasks
			WHERE tasks.id = :id
		");
		$stm->execute([
			'id' => $id,
		]);

		$stm->setFetchMode(\PDO::FETCH_CLASS, Task::class);

		return $stm->fetch();
	}

	public function all() {
		$stm = $this->db->query('
			SELECT id, description, due, complete
			FROM tasks
		');
		$stm->setFetchMode(\PDO::FETCH_CLASS, Task::class);

		return $stm->fetchAll();
	}

	protected function buildColumns(Task $task, array $additionnal = []) {

		return array_merge([
			'description' => $task->getDescription(),
			'due' => $task->getDue()->format('Y-m-d H:i:s'),
			'complete' => $task->getComplete() ? 1 : 0,
		], $additionnal);
	}

}