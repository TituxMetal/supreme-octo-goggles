<?php

use Todo\TaskManager;

use Todo\Models\Task;
use Todo\Storage\MySqlDatabaseTaskStorage;

require 'vendor/autoload.php';

$db = new PDO('mysql:host=localhost;dbname=tasky', 'root', 'root');

$storage = new MySqlDatabaseTaskStorage($db);

$manager = new TaskManager($storage);