<?php

namespace Todo\Models;

use DateTime;

class Task {

	protected $id;
	protected $complete = false;
	protected $description;
	protected $due;

	public function setDescription($description) {
		if(!is_string($description) || strlen($description) > 256) {
			throw new \Exception("Error description must be a string and maximum 255 characters length");
		}

		$this->description = $description;

		return $this;
	}

	public function getDescription() {

		return $this->description;
	}

	public function setComplete($complete = true) {
		$this->complete = $complete;

		return $this;
	}

	public function getComplete() {

		return (bool) $this->complete;
	}

	public function setDue(DateTime $due) {
		$this->due = $due;

		return $this;
	}

	public function getDue() {
		if(!$this->due instanceof DateTime) {

			return new DateTime($this->due);
		}

		return $this->due;
	}

	public function getId() {

		return $this->id;
	}

}